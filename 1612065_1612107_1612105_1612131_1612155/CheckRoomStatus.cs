﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1612065_1612107_1612105_1612131_1612155
{
    public partial class CheckRoomStatus : Form
    {
        String maKS = "B084";
        public CheckRoomStatus()
        {
            InitializeComponent();

            List<DTO.RoomType> roomTypes = DAO.RoomTypeDAO.Instance.LoadRoomTypeByMaKS(maKS);

            comboBox1.DataSource = roomTypes;

            comboBox1.DisplayMember = "TenLoaiPhong";

            KSName.Text = DAO.HotelDAO.Instance.GetHotelNameByMaKS(maKS);
        }

        private void CheckBt_Click(object sender, EventArgs e)
        {
            string theDate = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            DTO.RoomType type = comboBox1.SelectedItem as DTO.RoomType;
            dataGridView1.DataSource = DAO.DataProvider.Instance.ExecuteQuery("exec Show_Room_Stat @loaiPhong = N'"+type.MaLoaiPhong+"', @ngay = '" + theDate + "', @maKS = N'" + maKS + "'"); //2006-09-02
        }

        private void ExitBt_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


    }
}
