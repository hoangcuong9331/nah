﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace _1612065_1612107_1612105_1612131_1612155.DAO
{
    class RoomTypeDAO
    {
        private static RoomTypeDAO instance;

        public static RoomTypeDAO Instance
        {
            get { if (instance == null) instance = new RoomTypeDAO(); return RoomTypeDAO.instance; }
            private set { RoomTypeDAO.instance = value; }
        }

        private RoomTypeDAO() { }

        public List<DTO.RoomType> LoadRoomTypeByMaKS(string maKS)
        {
            List<DTO.RoomType> roomTypeList = new List<DTO.RoomType>();

            DataTable data = DAO.DataProvider.Instance.ExecuteQuery("select * from LOAIPHONG where maKS = N'" + maKS +"'");

            foreach (DataRow item in data.Rows)
            {
                DTO.RoomType roomType = new DTO.RoomType(item);
                roomTypeList.Add(roomType);
            }

            return roomTypeList;
        }
    }
}
