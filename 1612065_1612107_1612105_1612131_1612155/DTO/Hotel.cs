﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace _1612065_1612107_1612105_1612131_1612155.DTO
{
    class Hotel
    {
        public Hotel(DataRow row)
        {

        }

        private string maKS;
        public string MaKS
        {
            get { return maKS; }
            set { maKS = value; }
        }
        private string tenKS;
        public string TenKS
        {
            get { return tenKS; }
            set { tenKS = value; }
        }
        private string soNha;
        public string SoNha
        {
            get { return soNha; }
            set { soNha = value; }
        }
        private string duong;
        public string Duong
        {
            get { return duong; }
            set { duong = value; }
        }
        private string quan;
        public string Quan
        {
            get { return quan; }
            set { quan = value; }
        }
        private string thanhPho;
        public string ThanhPho
        {
            get { return thanhPho; }
            set { thanhPho = value; }
        }
        private int soSao;
        public int SoSao
        {
            get { return soSao; }
            set { soSao = value; }
        }
        private decimal giaTB;
        public decimal GiaTB
        {
            get { return giaTB; }
            set { giaTB = value; }
        }
        private string moTa;
        public string MoTa
        {
            get { return moTa; }
            set { moTa = value; }
        }
    }
}
