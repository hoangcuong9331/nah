﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace _1612065_1612107_1612105_1612131_1612155.DTO
{
    class RoomType
    {
        public RoomType(DataRow row)
        {
            this.MaLoaiPhong = row["maLoaiPhong"].ToString();
            this.TenLoaiPhong = row["tenLoaiPhong"].ToString();
            this.MaKS = row["MaKS"].ToString();
            this.Dongia = (decimal)row["dongia"];
            this.SlTrong = (int)row["slTrong"];
            this.MoTa = row["moTa"].ToString();
        }

        private string maLoaiPhong;
        public string MaLoaiPhong
        {
            get { return maLoaiPhong; }
            set { maLoaiPhong = value; }
        }

        private string tenLoaiPhong;
        public string TenLoaiPhong
        {
            get { return tenLoaiPhong; }
            set { tenLoaiPhong = value; }
        }

        private string maKS;
        public string MaKS
        {
            get { return maKS; }
            set { maKS = value; }
        }

        private decimal dongia;
        public decimal Dongia
        {
            get { return dongia; }
            set { dongia = value; }
        }
        private string moTa;
        public string MoTa
        {
            get { return moTa; }
            set { moTa = value; }
        }
        private int slTrong;
        public int SlTrong
        {
            get { return slTrong; }
            set { slTrong = value; }
        }
    }
}
